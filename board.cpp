#include "board.h"

Board::Board() {
    board = new Matrix(10, 10);
    figuresFactory = Factory::instance();
    threeFigures = new FigureToBoardInterface*[3];
    score = 0;
}

void Board::newGame() {
    delete board;
    delete [] threeFigures;
    board = new Matrix(10, 10);
    threeFigures = new FigureToBoardInterface*[3];
    score = 0;
    setFigures();
}
void Board::setFigures() {
    for (int i = 0; i < 3; ++i) {
        threeFigures[i] = figuresFactory->getRandomFigure();
    }
    figuresInArray = 3;
}
bool Board::putFigureOnBoard(int x, int y, int figureId) {
    int insertingResult = board->setFigure(x, y, threeFigures[figureId]);
    if ( insertingResult != -1 ) {
        score += insertingResult;
        figuresInArray--;
        delete threeFigures[figureId];
        threeFigures[figureId] = NULL;
        if (figuresInArray == 0) setFigures();
        return true;
    }
    return false;
}

int Board::getScore() {
    return score;
}
