#ifndef BOARD_H
#define BOARD_H
#include "matrix.h"
#include "factory.h"


class Board
{
private:
    Matrix *board;
    Factory* figuresFactory;
    FigureToBoardInterface **threeFigures;
    int score, figuresInArray;
    void setFigures();
public:
    void newGame();
    bool putFigureOnBoard(int x, int y, int figureId);
    void printAll() {
        std::cout<<"score = "<<score << std::endl;
//        board->printF();
        std::cout<< "=============\n";
        for (int i = 0; i < 3; ++i) {
            if (threeFigures[i]) {
                std::cout<< "figure id = "<< i <<std::endl;
                threeFigures[i]->printF();
                std::cout<<"=============\n";
            }
        }
    }
    QVector< QVector<bool> > getBoard() {
        return board->getBoard();
    }
    FigureToBoardInterface** getFigures() {
        FigureToBoardInterface** result;
        result = new FigureToBoardInterface*[3];
        for (int i = 0; i < 3; ++i) {
            result[i] = threeFigures[i];
        }
        return result;
    }
    int getScore();
    Board();
};

#endif // BOARD_H
