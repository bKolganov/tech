#include "factory.h"
#include <QTime>
Factory* Factory::_self = NULL;

Factory::Factory() {
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
}

Factory* Factory::instance() {
    if (!_self) {
        _self = new Factory;
    }
    return _self;
}

bool Factory::deleteInstance() {
    if (_self) {
        delete _self;
        return true;
    }
    return false;
}

// squares begin
FigureToBoardInterface* Factory::generateSmallSquare() {
    FigureToBoardInterface* result = new Square;
    return result;
}
FigureToBoardInterface* Factory::generateBigSquare() {
    FigureToBoardInterface* result = new Square(false);
    return result;
}
// squares end

// vert lines begin
FigureToBoardInterface* Factory::generateDot() {
    FigureToBoardInterface* result = new Line;
    return result;
}
FigureToBoardInterface* Factory::generateV2Line() {
    FigureToBoardInterface* result = new Line(2);
    return result;
}
FigureToBoardInterface* Factory::generateV3Line() {
    FigureToBoardInterface* result = new Line(3);
    return result;
}
FigureToBoardInterface* Factory::generateV4Line() {
    FigureToBoardInterface* result = new Line(4);
    return result;
}
FigureToBoardInterface* Factory::generateV5Line() {
    FigureToBoardInterface* result = new Line(5);
    return result;
}
// vert lines end

// hor line begin
FigureToBoardInterface* Factory::generateH2Line() {
    FigureToBoardInterface* result = new Line(2, false);
    return result;
}
FigureToBoardInterface* Factory::generateH3Line() {
    FigureToBoardInterface* result = new Line(3, false);
    return result;
}
FigureToBoardInterface* Factory::generateH4Line() {
    FigureToBoardInterface* result = new Line(4, false);
    return result;
}
FigureToBoardInterface* Factory::generateH5Line() {
    FigureToBoardInterface* result = new Line(5, false);
    return result;
}
// hor lines end

// angles begin
FigureToBoardInterface* Factory::generateRightUpAngle() {
    FigureToBoardInterface* result = new Angle;
//    result->printF();
    return result;
}
FigureToBoardInterface* Factory::generateRightDownAngle() {
    FigureToBoardInterface* result = new Angle(true, false);
    return result;
}
FigureToBoardInterface* Factory::generateLeftUpAngle() {
    FigureToBoardInterface* result = new Angle(false);
//    result->printF();
    return result;
}
FigureToBoardInterface* Factory::generateLeftDown() {
    FigureToBoardInterface* result = new Angle(false, false);
    return result;
}
//angles end

// random Figure by types
FigureToBoardInterface* Factory::getRandomAngle() {
    int rndInt = qrand() % 4;
    switch (rndInt) {
    case 0:
        return generateRightUpAngle();
    case 1:
        return generateRightDownAngle();
    case 2:
        return generateLeftUpAngle();
    case 3:
        return generateLeftDown();
    }
}

FigureToBoardInterface* Factory::getRandomSquare() {
    int rndInt = qrand() % 2;
    switch (rndInt) {
    case 0:
        return generateSmallSquare();
    case 1:
        return generateBigSquare();
    }
}

FigureToBoardInterface* Factory::getRandomLine() {
    int rndInt = qrand() % 9;
    switch (rndInt) {
    case 0:
        return generateDot();
    case 1:
        return generateV2Line();
    case 2:
        return generateV3Line();
    case 3:
        return generateV4Line();
    case 4:
        return generateV5Line();
    case 5:
        return generateH2Line();
    case 6:
        return generateH3Line();
    case 7:
        return generateH4Line();
    case 8:
        return generateH5Line();
    }
}

FigureToBoardInterface* Factory::getRandomFigure() {
    int rndInt = qrand() % 15;
    int indexArray[] = {0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2};
    switch (indexArray[rndInt]){
    case 0:
        return getRandomSquare();
    case 1:
        return getRandomAngle();
    case 2:
        return getRandomLine();
    }
}

