#ifndef FACTORY_H
#define FACTORY_H
#include "figure.h"

class Factory {
private:
    FigureToBoardInterface* generateSmallSquare();
    FigureToBoardInterface* generateBigSquare();
    FigureToBoardInterface* generateDot();
    FigureToBoardInterface* generateV2Line();
    FigureToBoardInterface* generateV3Line();
    FigureToBoardInterface* generateV4Line();
    FigureToBoardInterface* generateV5Line();
    FigureToBoardInterface* generateH2Line();
    FigureToBoardInterface* generateH3Line();
    FigureToBoardInterface* generateH4Line();
    FigureToBoardInterface* generateH5Line();
    FigureToBoardInterface* generateRightUpAngle();
    FigureToBoardInterface* generateRightDownAngle();
    FigureToBoardInterface* generateLeftUpAngle();
    FigureToBoardInterface* generateLeftDown();
protected:
    static Factory* _self;
    Factory();
public:
    static Factory* instance();
    static bool deleteInstance();
    FigureToBoardInterface* getRandomSquare();
    FigureToBoardInterface* getRandomLine();
    FigureToBoardInterface* getRandomAngle();
    FigureToBoardInterface* getRandomFigure();

};

#endif // FACTORY_H
