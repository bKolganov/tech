#include "figure.h"


Figure::Figure(int h, int w, bool **fM) {
    figureMatrix = new bool*[h];
    height = h;
    width = w;
    for (int i = 0; i < h; ++i) {
        figureMatrix[i] = new bool[w];
        for (int j = 0; j < w; ++j) {
           figureMatrix[i][j] = fM[i][j];
        }
    }
}

Figure* FigureToBoardInterface::getFigure() {
    Figure *tmp = new Figure(height, width, figureMatrix);
    return tmp;
}

Angle::Angle(bool right, bool up) {
    height = 3;
    width = 3;
    figureMatrix = new bool* [height];
    for (int i = 0; i < height; ++i) {
        figureMatrix[i] = new bool [width];
        if ( right ) figureMatrix[i][0] = true;
        else figureMatrix[i][2] = true;
    }
    for (int i = 0; i < width; ++i) {
        if ( up ) figureMatrix[2][i] = true;
        else figureMatrix[0][i] = true;
    }

}

void Line::initAndFillArray(int h, int w) {
    figureMatrix = new bool*[h];
    if (h == 1) {
        figureMatrix[0] = new bool[w];
        for (int i = 0; i < w; ++i) {
            figureMatrix[0][i] = true;
        }
    } else {
        for (int i = 0; i < h; ++i) {
            figureMatrix[i] = new bool(true);
        }
    }
}

Line::Line(int lenght, bool vertical) {
    if ( vertical ) {
        width = 1;
        height = lenght;
    } else {
        width = lenght;
        height = 1;
    }
    initAndFillArray(height, width);
}

void Square::initAndFillArray(int h, int w) {
    figureMatrix = new bool*[h];
    for (int i = 0; i < h; ++i) {
        figureMatrix[i] = new bool[w];
        for (int j = 0; j < w; ++j) {
            figureMatrix[i][j] = true;
        }
    }
}

Square::Square(bool small) {
    if ( small ) {
        height = 2;
        width = 2;
    } else {
        height = 3;
        width = 3;
    }
    initAndFillArray(height, width);
}

Figure* Figure::operator=(const Figure &f) {
    height = f.height;
    width = f.width;
    figureMatrix = new bool*[height];
    for (int i = 0; i < height; ++i) {
        figureMatrix[i] = new bool[width];
        for (int j = 0; j < width; ++j) {
           figureMatrix[i][j] = f.figureMatrix[i][j];
        }
    }
}
