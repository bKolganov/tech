#ifndef FIGURE_H
#define FIGURE_H
#include <iostream>
struct Figure
{
    int height, width;
    int idColor;
    bool** figureMatrix;
    Figure(int h, int w, bool** fM);
    Figure() {}
    //Figure(const Figure &f);
    virtual ~Figure() {
        for (int i = 0; i < height; ++i) {
            delete [] figureMatrix[i];
        }
        delete figureMatrix;
    }
    Figure* operator=(const Figure& f);
};

class FigureToBoardInterface : protected Figure {
public:
    void printF() {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (figureMatrix[i][j]) std::cout << "# ";
                else std::cout << "_ ";
            }
            std::cout << std::endl;
        }
//        std::cout << "\n";
    };
    virtual Figure* getFigure();
};

class Angle : public FigureToBoardInterface {
public:
    Angle(bool right = true, bool up = true);
};

class Line : public FigureToBoardInterface {
private:
    void initAndFillArray(int h, int w);
public:
    Line(int lenght = 1, bool vertical = true);
};

class Square : public FigureToBoardInterface {
private:
    void initAndFillArray(int h, int w);
public:
    Square(bool small = true);
};

#endif // FIGURE_H
