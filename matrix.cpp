#include "matrix.h"

Matrix::Matrix(int h, int w)
{
    board = new bool*[h];

    height = h;
    width = w;
    for (int i = 0; i < h; ++i) {
        board[i] = new bool[w];
        for (int j = 0; j < w; ++j) board[i][j] = false;
    }
}

void Matrix::checkRows(QVector<int> &rowsForDelete) {
   for (int i = 0; i < height; ++i) {
       for (int j = 0; j < width; ++j) {
           if (!board[i][j]) {
               break;
           }
           if( j == width - 1 ) {
               rowsForDelete.push_back(i);
           }
       }
   }
}

void Matrix::checkCols(QVector<int> &colsForDetele) {
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            if (!board[j][i]) break;
            if(j == width - 1) {
                colsForDetele.push_back(i);
            }
        }
    }
}
int Matrix::deleteRowOrCols(int n, bool row) {
    if ( row ) {
        for (int i = 0; i < width; ++i) {
            board[n][i] = false;
        }
    }
    else {
        for (int i = 0; i < height; ++i) {
            board[i][n] = false;
        }
    }
    return 10;
}

int Matrix::deleteFullLines() {
    int score = 0;
    QVector<int> colsForDelete;
    QVector<int> rowsForDelete;
    checkCols(colsForDelete);
    checkRows(rowsForDelete);
    if (!colsForDelete.empty() && !rowsForDelete.empty()) {
        score = colsForDelete.size() * rowsForDelete.size() * 100;
    } else {
        score = colsForDelete.size() + rowsForDelete.size();
        score *= 10;
    }
    for (int i = 0; i < colsForDelete.size(); ++i) {
        deleteRowOrCols(colsForDelete[i], false);
    }
    for (int i = 0; i < rowsForDelete.size(); ++i) {
        deleteRowOrCols(rowsForDelete[i], true);
    }
    return score;
}

bool Matrix::checkFreeSpace(int x, int y, Figure *f) {
    bool result = true;
    int fw = f->width;
    int fh = f->height;
    if ((x + fw > 10) || (y + fh > 10)) return false;
    for (int i = 0; (i < fw); ++i ) {
        for ( int j = 0; (j < fh); ++j) {
            result = !(board[j + y][i + x] && f->figureMatrix[j][i]);
            if (!result) return result;
        }
    }
    return result;
}

int Matrix::setFigure(int x, int y, FigureToBoardInterface *f) {
    if (checkFreeSpace(x, y, f->getFigure())) {
        Figure *fig = f->getFigure();
        int fw = fig->width;
        int fh = fig->height;
        for (int i = 0; (i < fw); ++i ) {
            for ( int j = 0; (j < fh); ++j) {
                board[j + y][i + x] = fig->figureMatrix[j][i] || board[j + y][i + x];
            }
        }
        return deleteFullLines() + 5;
    } else {
        return -1;
    };
}

QVector<QVector<bool> > Matrix::getBoard() {
   QVector<QVector<bool> > result(height);
   for (int i = 0; i < height; ++i) {
       result[i].resize(width);
       for (int j = 0; j < width; ++j) {
           result[i][j] = board[i][j];
       }
   }
   return result;
}

