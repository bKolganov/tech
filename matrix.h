#ifndef MATRIX_H
#define MATRIX_H
#include "figure.h"
#include <QVector>

class Matrix
{
private:
    bool** board;
    int height, width;
    void checkRows(QVector<int> &rowsForDelete);
    void checkCols(QVector<int> &colsForDelete);
    int deleteRowOrCols(int n, bool row = true);
    bool checkFreeSpace(int x, int y, Figure *f);
public:
    int deleteFullLines();
//    void printF() {
//        std::cout<< "0 1 2 3 4 5 6 7 8 9\n";
//        for (int i = 0; i < height; ++i) {
//            for (int j = 0; j < width; ++j) {
//                if (board[i][j]) std::cout << "# ";
//                else std::cout << "_ ";
//            }
//            std::cout << i <<std::endl;
//        }
////        std::cout << "\n";
//    }
    QVector< QVector<bool> > getBoard();
    int setFigure(int x, int y, FigureToBoardInterface *f);
    Matrix(int h, int w);
    ~Matrix() {
        for (int i = 0; i < height; ++i) {
            delete board[i];
        }
        delete board;
    };
};

#endif // MATRIX_H
