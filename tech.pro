#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T21:25:47
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = tech
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    figure.cpp \
    factory.cpp \
    board.cpp \
    matrix.cpp

HEADERS += \
    figure.h \
    factory.h \
    board.h \
    matrix.h
